require "lib.slam"
vector = require "lib.hump.vector"
tlfres = require "lib.tlfres"
local class = require 'lib.middleclass' -- see https://github.com/kikito/middleclass

require "lib.helpers"

CANVAS_WIDTH = 1920
CANVAS_HEIGHT = 1080
headerHeight = 200
footerHeight = 100
screen = "title"

function love.load()
    -- set up default drawing options
    love.graphics.setBackgroundColor(0, 0, 0)
    love.keyboard.setKeyRepeat(true)

    -- load assets
    images = {}
    for i,filename in pairs(love.filesystem.getDirectoryItems("images")) do
        if filename ~= ".gitkeep" then
            images[filename:sub(1,-5)] = love.graphics.newImage("images/"..filename)
        end
    end

    sounds = {}
    for i,filename in pairs(love.filesystem.getDirectoryItems("sounds")) do
        if filename ~= ".gitkeep" then
            sounds[filename:sub(1,-5)] = love.audio.newSource("sounds/"..filename, "static")
        end
    end
    sounds.step:setVolume(0.1)
    sounds.blocked:setVolume(0.3)

    music = {}
    for i,filename in pairs(love.filesystem.getDirectoryItems("music")) do
        if filename ~= ".gitkeep" then
            music[filename:sub(1,-5)] = love.audio.newSource("music/"..filename, "stream")
            music[filename:sub(1,-5)]:setLooping(true)
        end
    end
    musicVolume = 0.1
    music.track1:setVolume(0.1)
    love.audio.play(music.track1)

    fonts = {}
    font = love.graphics.newFont("fonts/".."lavi.ttf", 80)
    smallFont = love.graphics.newFont("fonts/".."lavi.ttf", 30)
    titleFont = love.graphics.newFont("fonts/".."munstar.ttf", 50)
    titleBigFont = love.graphics.newFont("fonts/".."munstar.ttf", 120)
    
    -- for i,filename in pairs(love.filesystem.getDirectoryItems("fonts")) do
        -- if filename ~= ".gitkeep" then
        --     fonts[filename:sub(1,-5)] = {}
        --     for fontsize=50,100,50 do
        --         fonts[filename:sub(1,-5)][fontsize] = love.graphics.newFont("fonts/"..filename, fontsize)
        --     end
        -- end
    -- end

    -- Game-specific stuff:
    levels = {}
    for i,filename in ipairs(love.filesystem.getDirectoryItems("levels")) do
        if filename:sub(1,1):match("%d") then
            local level = require("levels."..filename:sub(1,-5))
            level.title = filename:sub(1,2) .. ": " .. filename:sub(4,-5):gsub("-", " ")
            level.outro = level.outro or "Bunny got all the carrots! (Press space to flip the page.)"
            table.insert(levels, level)
        end
    end
    -- font = fonts.lavi[100]
    -- smallFont = love.graphics.newFont(30)
end

function love.update(dt)
end

function love.mouse.getPosition()
    return tlfres.getMousePosition(CANVAS_WIDTH, CANVAS_HEIGHT)
end

function love.keypressed(key)
    if key == "f" then
        isFullscreen = love.window.getFullscreen()
        love.window.setFullscreen(not isFullscreen)
    end

    if key == "m" then
        if musicVolume == 0 then
            musicVolume = 0.1
        else
            musicVolume = 0
        end
        music.track1:setVolume(musicVolume)

    end

    if screen == "game" then
        if key == "escape" then
            love.window.setFullscreen(false)
            love.event.quit()
        end

        if hasWon then
            if key == "space" then
                loadNextLevel()
                return
            end
        else
            if key == "up" then
                movePlayer(playerX, playerY, 0, -1, true)
            elseif key == "down" then
                movePlayer(playerX, playerY, 0, 1, true)
            elseif key == "left" then
                movePlayer(playerX, playerY, -1, 0, true)
            elseif key == "right" then
                movePlayer(playerX, playerY, 1, 0, true)
            elseif key == "space" then
                movePlayer(playerX, playerY, 0, 0, true)
            end
        end

        if key == "0" then
            loadNextLevel()
        elseif key == "9" then
            loadPreviousLevel()
        elseif key == "z" or key == "backspace" or key == "y" or key == "u" then
            undo()
        elseif key == "r" then
            reset()
        end
    elseif screen == "title" then
        if key == "escape" then
            love.window.setFullscreen(false)
            love.event.quit()
        elseif key == "space" or key == "0" or key == "9" then
            screen = "game"
            currentLevel = 1
            loadLevel(currentLevel)
        end
    elseif screen == "credits" then
        if key == "escape" or key == "space" then
            screen = "title"
        end
    end
end

function love.mousepressed(x, y, button)
    if screen == "title" then
        screen = "game"
        currentLevel = 1
        loadLevel(currentLevel)
    end
end

function movePlayer(x, y, dx, dy, isActive)
    isActive = isActive or false

    if x+dx < 1 or x+dx > GRID_WIDTH or y+dy < 1 or y+dy > GRID_HEIGHT then
        sounds.blocked:setPitch(1+0.4*math.random())
        sounds.blocked:play()
        return false
    end

    if grid[x+dx][y+dy] == "stone" then
        sounds.blocked:setPitch(1+0.4*math.random())
        sounds.blocked:play()
        return false
    end

    if isActive then
        pushCurrentStateToUndoStack()
    end

    local panelBeforeMove = findPanel(x, y)
    local panelAfterMove = findPanel(x + dx, y + dy)
    -- panelAfterMove is just the regular movement without passing of time.

    if panelBeforeMove ~= panelAfterMove then
        sounds.travel:setPitch(0.6+0.4*math.random())
        sounds.travel:play()
    end

    if isActive then
        if panelBeforeMove == panelAfterMove and panelBeforeMove ~= nil then
            if panelAfterMove == #panels then
                if isActive then
                    sounds.blocked:setPitch(1+0.4*math.random())
                    sounds.blocked:play()
                    undo()
                end
                return false
            end
            nextPanel = panelAfterMove + 1
            x = x + panels[nextPanel].x - panels[panelBeforeMove].x
            y = y + panels[nextPanel].y - panels[panelBeforeMove].y
        end
    end
    local targetPanel = findPanel(x + dx, y + dy)
    -- targetPanel is the panel the player ends up in.
    if grid[x+dx][y+dy] == "carrot" then
        relX = (x+dx) - panels[targetPanel].x
        relY = (y+dy) - panels[targetPanel].y
        for i = targetPanel, #panels do
            p = panels[i]
            if grid[p.x + relX][p.y + relY] == "carrot" then
                grid[p.x + relX][p.y + relY] = "empty"
            end
        end
        sounds.carrot:setPitch(1+0.4*math.random())
        sounds.carrot:play()
    end

    if grid[x+dx][y+dy] == "bunny" then
        if dx == 0 and dy == 0 then
            sounds.blocked:setPitch(1+0.4*math.random())
            sounds.blocked:play()
            undo()
            return false
        end
        if not movePlayer(x+dx, y+dy, dx, dy) then
            if isActive then
                sounds.blocked:setPitch(1+0.4*math.random())
                sounds.blocked:play()
                undo()
            end
            return false
        end
    end

    if panelBeforeMove ~= panelAfterMove or panelBeforeMove == nil then
        grid[x][y] = "empty"
    end
    x = x + dx
    y = y + dy
    grid[x][y] = "bunny"

    sounds.step:setPitch(1+0.4*math.random())
    sounds.step:play()

    if noCarrotsInLastPanel() then
        hasWon = true
        sounds.win:play()
    end

    if isActive then
        playerX = x
        playerY = y
    end

    return true
end

function findPanel(x, y)
    for i,panel in ipairs(panels) do
        if x >= panel.x and x < panel.x + panel.w and
           y >= panel.y and y < panel.y + panel.h then
            return i
        end
    end
    return nil
end

function love.draw()
    love.graphics.setColor(1, 1, 1)
    tlfres.beginRendering(CANVAS_WIDTH, CANVAS_HEIGHT)

    if screen == "title" then
        --love.graphics.setColor(1, 1, 1)
        --love.graphics.rectangle("fill", 0, 0, CANVAS_WIDTH, CANVAS_HEIGHT)
        --love.graphics.setColor(0, 0, 0)
        --love.graphics.setFont(titleBigFont)
        --love.graphics.printf(string.lower("Carrot Conundrum"), 0, 300, CANVAS_WIDTH, "center")
        --love.graphics.setFont(titleFont)
        --love.graphics.printf(string.lower("The incredible story of the bunny who hopped through time!"), 0, 500, CANVAS_WIDTH, "center")
        --love.graphics.printf(string.lower("Press space to start"), 0, 700, CANVAS_WIDTH, "center")

        -- center full screen:
        love.graphics.draw(images.title, CANVAS_WIDTH/2-images.title:getWidth()/2, CANVAS_HEIGHT/2-images.title:getHeight()/2)
    elseif screen == "credits" then
        love.graphics.draw(images.back, CANVAS_WIDTH/2-images.back:getWidth()/2, CANVAS_HEIGHT/2-images.back:getHeight()/2)
        --love.graphics.setColor(1, 1, 1)
        --love.graphics.rectangle("fill", 0, 0, CANVAS_WIDTH, CANVAS_HEIGHT)
        --love.graphics.setColor(0, 0, 0)
        --love.graphics.setFont(titleBigFont)
        --love.graphics.printf(string.lower("Thanks for playing! <3"), 0, 300, CANVAS_WIDTH, "center")
        --love.graphics.setFont(titleFont)
        --love.graphics.printf(string.lower("Press escape to return to the title screen."), 0, 500, CANVAS_WIDTH, "center")
        --love.graphics.printf(string.lower("(Hint: You can press 0 and 9 to flip through the pages!)"), 0, 600, CANVAS_WIDTH, "center")
    elseif screen == "game" then
        love.graphics.setLineWidth(math.min(CELL_SIZE/40, 5))

        love.graphics.push()
        local pageHeight = GRID_HEIGHT * CELL_SIZE + 2*PANEL_PADDING + headerHeight + footerHeight
        love.graphics.translate((CANVAS_WIDTH-GRID_WIDTH * CELL_SIZE)/2, PANEL_PADDING + headerHeight + (CANVAS_HEIGHT-pageHeight)/2)

        -- Draw the page.
        love.graphics.setColor(1, 1, 1)
        love.graphics.rectangle("fill", -PANEL_PADDING, -PANEL_PADDING - headerHeight , GRID_WIDTH * CELL_SIZE + 2*PANEL_PADDING, pageHeight)

        -- Draw the header.
        love.graphics.draw(images.logo, 10, -headerHeight+25, 0, 0.9, 0.9)
        love.graphics.setFont(titleFont)
        love.graphics.setColor(0, 0, 0)
        love.graphics.printf(string.lower(title), 20+3+450, -headerHeight + 20+3, GRID_WIDTH * CELL_SIZE + 2*PANEL_PADDING, "left")
        --love.graphics.setColor(0.1, 0.1, 0.9)
        --love.graphics.printf(string.lower(title), 20, -headerHeight + 20, GRID_WIDTH * CELL_SIZE + 2*PANEL_PADDING, "left")

        love.graphics.setColor(242/255, 220/255, 107/255)
        local introOffset = PANEL_PADDING
        love.graphics.rectangle("fill",  introOffset, -PANEL_PADDING - headerHeight + 100 , GRID_WIDTH * CELL_SIZE - 2*introOffset,  headerHeight - 100 - PANEL_PADDING)

        love.graphics.setColor(0, 0, 0)
        love.graphics.rectangle("line",  introOffset, -PANEL_PADDING - headerHeight + 100 , GRID_WIDTH * CELL_SIZE - 2*introOffset,  headerHeight - 100 - PANEL_PADDING)

        love.graphics.setFont(smallFont)
        love.graphics.setColor(0, 0, 0)
        local messageOffset = introOffset
        love.graphics.printf(message, introOffset + messageOffset, -headerHeight + 100, GRID_WIDTH * CELL_SIZE - 2*introOffset - 2*messageOffset, "left")


        -- Draw the footer.
        if hasWon or alwaysWin then
            love.graphics.setColor(0.5, 0.5, 0.9)
            local introOffset = PANEL_PADDING
            love.graphics.rectangle("fill",  introOffset, GRID_HEIGHT * CELL_SIZE + 2*PANEL_PADDING + headerHeight - 2*footerHeight, GRID_WIDTH * CELL_SIZE - 2*introOffset, footerHeight - 20)
            love.graphics.setColor(0, 0, 0)
            love.graphics.rectangle("line",  introOffset, GRID_HEIGHT * CELL_SIZE + 2*PANEL_PADDING + headerHeight - 2*footerHeight, GRID_WIDTH * CELL_SIZE - 2*introOffset, footerHeight - 20)

            love.graphics.setFont(smallFont)
            love.graphics.printf(outro, introOffset + messageOffset, GRID_HEIGHT * CELL_SIZE + 2*PANEL_PADDING + headerHeight - 2*footerHeight + introOffset, GRID_WIDTH * CELL_SIZE - 2*introOffset - 2*messageOffset, "left")
        end

        -- Draw the panels
        for i, p in ipairs(panels) do
            love.graphics.setColor(127/255.0, 156/255.0, 61/255.0)
            love.graphics.rectangle("fill", (p.x-1)*CELL_SIZE + PANEL_PADDING, (p.y-1)*CELL_SIZE + PANEL_PADDING, CELL_SIZE*p.w - 2*PANEL_PADDING, CELL_SIZE*p.h - 2*PANEL_PADDING)
            love.graphics.setColor(0, 0, 0)
            love.graphics.rectangle("line", (p.x-1)*CELL_SIZE + PANEL_PADDING, (p.y-1)*CELL_SIZE + PANEL_PADDING, CELL_SIZE*p.w - 2*PANEL_PADDING, CELL_SIZE*p.h - 2*PANEL_PADDING)
        end

        -- Mark the active player.
        love.graphics.setColor(1, 1, 1)
        love.graphics.draw(images.bunnyglow, (playerX-0.5)*CELL_SIZE, (playerY-0.5)*CELL_SIZE, 0, CELL_SIZE/250, CELL_SIZE/250, images.bunnyglow:getWidth()/2, images.bunnyglow:getHeight()/2)

        -- Draw the grid.
        for x = 1, GRID_WIDTH do
            for y = 1, GRID_HEIGHT do
                if grid[x][y] == "empty" then
                    --love.graphics.setColor(0, 0, 0)
                    --love.graphics.rectangle("fill", (x-1)*CELL_SIZE, (y-1)*CELL_SIZE, CELL_SIZE, CELL_SIZE)
                elseif grid[x][y] == "carrot" then
                    love.graphics.draw(images.carrot, (x-0.5)*CELL_SIZE, (y-0.5)*CELL_SIZE, 0, CELL_SIZE/400, CELL_SIZE/400, images.carrot:getWidth()/2, images.carrot:getHeight()/2)
                elseif grid[x][y] == "bunny" then
                    love.graphics.draw(images.bunny, (x-0.5)*CELL_SIZE, (y-0.5)*CELL_SIZE, 0, CELL_SIZE/250, CELL_SIZE/250, images.bunny:getWidth()/2, images.bunny:getHeight()/2)
                elseif grid[x][y] == "stone" then
                    love.graphics.draw(images.stone, (x-0.5)*CELL_SIZE, (y-0.5)*CELL_SIZE, 0, CELL_SIZE/250, CELL_SIZE/250, images.stone:getWidth()/2, images.stone:getHeight()/2)
                else
                    error("Unknown grid cell type: "..grid[x][y])
                end
            end
        end

        -- Gray out non-active panels.
        for i, p in ipairs(panels) do
            if i ~= findPanel(playerX, playerY) then
                love.graphics.setColor(0, 0, 0, 0.3)
                love.graphics.rectangle("fill", (p.x-1)*CELL_SIZE + PANEL_PADDING, (p.y-1)*CELL_SIZE + PANEL_PADDING, CELL_SIZE*p.w - 2*PANEL_PADDING, CELL_SIZE*p.h - 2*PANEL_PADDING)
            end
        end

        love.graphics.pop()
        --if hasWon then
        --    local padding = 60
        --    local lineWidth = 5
        --    local rectangleHeight = 350 -- this depends on the winning text
        --    love.graphics.setColor(0, 0.3, 0.1, 0.5)
        --    love.graphics.rectangle("fill", CANVAS_WIDTH*0.1 - padding, CANVAS_HEIGHT/4 - padding, CANVAS_WIDTH*0.8 + padding*2, rectangleHeight, 20, 20)
        --    love.graphics.setLineWidth(10)
        --    love.graphics.setColor(1, 1, 1, 0.5)
        --    love.graphics.rectangle("line", CANVAS_WIDTH*0.1 - padding + lineWidth, CANVAS_HEIGHT/4 - padding + lineWidth, CANVAS_WIDTH*0.8 + padding*2 - lineWidth*2, rectangleHeight - lineWidth*2, 20, 20)
        --    -- piko was here /o\
        --    love.graphics.setColor(0.2, 0.2, 0.2, 0.5)
        --    love.graphics.rectangle("line", CANVAS_WIDTH*0.1 - padding, CANVAS_HEIGHT/4 - padding, CANVAS_WIDTH*0.8 + padding*2, rectangleHeight, 20, 20)
        --    love.graphics.setFont(font)
        --    love.graphics.setColor(0, 0, 0)
        --    love.graphics.printf(string.upper("You ate/will have eaten all the carrots! You win! Press space to go to the next level."), CANVAS_WIDTH*0.1, CANVAS_HEIGHT/4, CANVAS_WIDTH*0.8, "center")
        --end
    end

    tlfres.endRendering()
end

function loadLevel(i)
    hasWon = false
    local level = levels[i]
    message = level.intro
    outro = level.outro
    title = level.title

    local firstNewline, _ = level.content:find("\n")

    panels = {}

    PANEL_WIDTH = firstNewline - 1
    _, PANEL_HEIGHT = level.content:gsub("\n","")

    PANELS_X = level.panelsX
    PANELS_Y = level.panelsY
    MARGIN = level.margin or 0

    GRID_WIDTH = (PANEL_WIDTH+MARGIN)*PANELS_X + MARGIN
    GRID_HEIGHT = (PANEL_HEIGHT+MARGIN)*PANELS_Y + MARGIN
    PANEL_PADDING = math.min(CANVAS_WIDTH/GRID_WIDTH, CANVAS_HEIGHT/GRID_HEIGHT)/10
    CELL_SIZE = math.floor(math.min((CANVAS_WIDTH - 2*PANEL_PADDING)/GRID_WIDTH, ((CANVAS_HEIGHT-headerHeight-footerHeight) - 2*PANEL_PADDING)/GRID_HEIGHT))

    -- Load content.
    for y = MARGIN+1, GRID_HEIGHT, PANEL_HEIGHT+MARGIN do
        for x = MARGIN+1, GRID_WIDTH, PANEL_WIDTH+MARGIN do
            table.insert(panels, {x=x, y=y, w=PANEL_WIDTH, h=PANEL_HEIGHT})
            --grid[x + playerX - 1][y + playerY - 1] = "empty"
        end
    end

    playerX = nil
    playerY = nil

    grid = {}
    for x = 1, GRID_WIDTH do
        grid[x] = {}
        for y = 1, GRID_HEIGHT do
            grid[x][y] = "empty"
        end
    end

    local y = 1
    for line in level.content:gmatch("(.-)\n") do
        local x = 1
        for c in line:gmatch(".") do
            if c == "@" then
                grid[x + MARGIN][y + MARGIN] = "bunny"
                playerX = x + MARGIN
                playerY = y + MARGIN
            elseif c == "*" then
                placeInAllPanels(x, y, "carrot")
            elseif c == "#" then
                placeInAllPanels(x, y, "stone")
            elseif c == "." then
                -- No need to do anything.
            else
                error("Unknown character in level: "..c)
            end
            x = x + 1
        end
        y = y + 1
    end

    if not playerX or not playerY then
        error("No player in level!")
    end

    undoStack = {}
    pushCurrentStateToUndoStack()
end

function pushCurrentStateToUndoStack()
    table.insert(undoStack, {playerX=playerX, playerY=playerY, grid=deepcopy(grid)})
end

function undo()
    if #undoStack > 0 then
        local state = undoStack[#undoStack]
        playerX = state.playerX
        playerY = state.playerY
        grid = state.grid
        table.remove(undoStack)
    end
end

function reset()
    pushCurrentStateToUndoStack()
    local state = undoStack[1]
    playerX = state.playerX
    playerY = state.playerY
    grid = deepcopy(state.grid)
    hasWon = false
end

function placeInAllPanels(x, y, what)
    for i, p in ipairs(panels) do
        grid[x+p.x-1][y+p.y-1] = what
    end
end

function noCarrotsInLastPanel()
    for x = panels[#panels].x, GRID_WIDTH do
        for y = panels[#panels].y, GRID_HEIGHT do
            if grid[x][y] == "carrot" then
                return false
            end
        end
    end
    return true
end

function loadNextLevel()
    sounds.page:setPitch(0.8 + 0.4*math.random())
    sounds.page:play()

    if currentLevel == #levels then
        screen = "credits"
    else
        currentLevel = (currentLevel % #levels) + 1
        loadLevel(currentLevel)
    end
end

function loadPreviousLevel()
    sounds.page:setPitch(0.8 + 0.4*math.random())
    sounds.page:play()
    currentLevel = ((currentLevel-2) % #levels) + 1
    loadLevel(currentLevel)
end
