return {
    panelsX=5,
    panelsY=2,
    intro="When Bunny came to a set of two open doors, they entered the door on their left.",
    outro="This was not the correct way to the meeting room, and Bunny new it perfectly well...",
    content=[[
#*##*##
#**#*##
##*#*##
##.#.##
#.....#
#..@..#
]],
    }
