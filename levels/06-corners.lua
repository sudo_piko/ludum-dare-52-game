
return {
    panelsX=2,
    panelsY=2,
    intro="Bunny decided to use their new-found power to their advantage!",
    outro="Corners like these were cozy. Bunny felt like they had all the time in the world.",
    content=[[
*.*
.@.
*.*
]],
    }
