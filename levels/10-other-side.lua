return {
    panelsX=2,
    panelsY=3,
    intro="The carrots were always more orange on the other side. Bunny would have to find a way to get there.",
    outro="Bunny felt like they got a hang of this!",
    content=[[
....#****
..@.#****
....#****
]],
    }
