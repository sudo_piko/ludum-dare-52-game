return {
    panelsX=3,
    panelsY=3,
    intro="Sometimes, Bunny would get stuck in the last panel before they could eat all carrots. In those cases, they would need to retrace their steps and try again. (Press R to restart a level, and backspace to undo a move.)",
    outro="Bunny was getting better at this. They were starting to understand the rules of this world.",
    content=[[
#######
*######
.@****#
]],
    }
--##########
--.........#
--*#@******#
