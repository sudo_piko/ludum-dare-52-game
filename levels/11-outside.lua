return {
    panelsX=2,
    panelsY=2,
    margin=1,
    intro="Even with time travel, some challenges seemed impossible!",
    outro="Outside of the panels, untethered from time, Bunny felt strangely at home.",
    content=[[
######
#@****
#.####
]],
    }
