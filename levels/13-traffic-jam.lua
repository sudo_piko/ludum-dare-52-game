return {
    panelsX=4,
    panelsY=3,
    margin=1,
    intro="At night, ghosts of their past would haunt Bunny.",
    outro="But these ghosts didn't mean any harm, and Bunny found a way to reconcile with them.",
    content=[[
###*#######
###*#######
###*#######
@*#*#######
#**********
####*######
####*****##
###########
]],
    }
