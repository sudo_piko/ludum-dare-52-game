return {
    panelsX=3,
    panelsY=3,
    intro="Some pages were larger than others. The only thing that mattered to Bunny was to eat all carrots before the end of the page.",
    outro="So far so good. Bunny couldn't know that they would soon make a discovery that would change their life forever!",
    content=[[
.......
..***..
..*@*..
..***..
.......
]],
    }
