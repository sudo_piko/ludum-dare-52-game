
return {
    panelsX=3,
    panelsY=2,
    intro="Usually, Bunny would go as fast as they could! But sometimes, patience also paid off. (Press space to wait.)",
    outro="Bunny decided to take that as a lesson for other areas of their life.",
    content=[[
......
......
.@....
......
..**..
.****.
]],
    }
